from django.urls import path
from .views import *
from django.urls.conf import include
from django.contrib import admin




urlpatterns = [
    path('',index,name='index'),
    path('reserva/',reserva, name='reserva'),
    path('registro/', registro, name="registro"),
    path('pago/', pago, name='pago'),
    path('listado-reserva/', listado_reserva, name='listado_reserva'),
    path('eliminar-reserva/<id>/',eliminar_reserva, name='eliminar_reserva'),
    path('modificar-reserva/str(<id>)/', modificar_reserva, name="modificar_reserva"),
    path('calendariomedico/',calendariomedico, name="calendariomedico"),
    path('listado-pago/',listado_pago, name= "listado_pago" ),
    path('fullcalendar/',fullcalendar, name= "fullcalendar" ),


]
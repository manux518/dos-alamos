# Generated by Django 3.2.3 on 2021-06-23 22:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_agenda'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Registropago',
            new_name='pago',
        ),
    ]

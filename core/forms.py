from typing import ClassVar
from django import forms
from django.contrib import auth
from django.db import models
from django.db.models import fields
from django.forms import widgets    
from .models import *
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class ReservaForm(forms.ModelForm):

    class Meta:
        model = Reserva
        fields = '__all__'


class CustomUserCreationForm(UserCreationForm):
    pass


class pagoForm(forms.ModelForm):

    class Meta:
        model = Pago
        fields = '__all__'

class fullcalendarForm(forms.ModelForm):

    class Meta:
        model = fullcalendar
        fields = '__all__'
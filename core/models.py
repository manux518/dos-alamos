from io import open_code
from django.db import models
from django.db.models.base import ModelState
from django.db.models.fields import DateTimeField, NullBooleanField
from django.contrib.auth.models import User
from datetime import date



# Create your models here.

class prevision(models.Model):
    nombre = models.CharField(max_length=30)
    def __str__(self):
        return self.nombre

class sexo(models.Model):
    nombre = models.CharField(max_length=15)
    def __str__(self):
        return self.nombre

class especialidad(models.Model):
    nombre = models.CharField(max_length=50)
    def __str__(self):
        return self.nombre
class fecha_Atencion(models.Model):
    fecha = models.DateField()
    def __str__(self):
        return str(self.fecha)

class medico(models.Model):
    User = models.ForeignKey(User, on_delete=models.CASCADE,null=False,default=None)
    nombre = models.CharField(max_length=50,default=None, null=True)
    especialidad = models.ForeignKey(especialidad,on_delete=models.CASCADE)
    def __str__(self):
        return str(self.User)

class Reserva(models.Model):
    rut = models.CharField(max_length=9)
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    prevision = models.ForeignKey(prevision,on_delete=models.CASCADE,default=None,null=True)
    fecha_Atencion = models.ForeignKey(fecha_Atencion,on_delete=models.CASCADE,default=None)
    sexo = models.ForeignKey(sexo, on_delete=models.CASCADE,default=None)
    correo = models.EmailField()
    especialidad = models.ForeignKey(especialidad, on_delete=models.CASCADE,default=None,null=True)
    medico = models.ForeignKey(medico, on_delete=models.CASCADE,default=None,null=True)
    def __str__(self):
        return str(self.id)


class Pago(models.Model):
    Reserva = models.ForeignKey(Reserva, on_delete = models.CASCADE, default=None)
    Total_pago = models.IntegerField()
    fecha_pago = models.DateTimeField(default=date.today)
    def __str__(self):
        return str(self.id)

class agenda(models.Model):
    medico = models.ForeignKey(medico,on_delete = models.CASCADE, default=None)
    fecha_asignado = models.DateField(default=date.today)
    reserva = models.ForeignKey(Reserva,on_delete = models.CASCADE, default=None)
    def __str__(self):
        return str(self.medico)

class fullcalendar(models.Model):
    fecha = models.DateTimeField(default=date.today)
    descripcion = models.CharField(max_length=50)

from typing import get_args
from django.shortcuts import redirect, render, get_object_or_404
from .models import *
from .forms import *
from django.contrib.auth import authenticate, login
from django.contrib.messages.api import MessageFailure, success


# Create your views here.

def index(request):
    return render(request,'core/index.html')


def reserva(request):

    data = {
        'form': ReservaForm(),
    }

    if request.method == 'POST':
        formuluario = ReservaForm(data=request.POST)
        if formuluario.is_valid():
            formuluario.save()
            data["mensaje"] = "Reserva guardada correctamente"
        else:
            data["form"] = formuluario
            messages.success(request, "Reserva guardad correctamente")
            return render(request, 'core/reservahora.html', data)


def registro(request):
    data ={
        'form': CustomUserCreationForm()
    }

    if request.method == 'POST':
        formulario = CustomUserCreationForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            user = authenticate(username=formulario.cleaned_data["username"], password =formulario.cleaned_data ["password1"])
            login(request, user)
            #redirigir al home
            return redirect(to=index)
    return render(request, 'registration/registro.html', data)


def pago(request):

    data = {
        'form': pagoForm(),
    }

    if request.method == 'POST':
        formularioo = pagoForm(data=request.POST)
        if formularioo.is_valid():
            formularioo.save()
            data["mensaje"] = "Pago exitoso"
        else:
            data["form"] = formularioo
    return render(request, 'core/registropago.html', data)

def listado_reserva(request):
    reserva = Reserva.objects.all()
    data = {
        'reserva': reserva
    }

    return render(request, 'core/listado_reserva.html', data)


def modificar_reserva(request, id):
    Reserv = get_object_or_404(Reserva, id=id)
    data={
        'form': ReservaForm(instance=Reserv)
    }
    if request.method == 'POST ':
        formulario = ReservaForm(data=request.POST, instance=Reserv)
        if formulario.is_valid():
            formulario.save()
            return redirect(to="listado_reserva")
        else:
         data["form"] = formulario
    return render(request, 'core/modificar_reserva.html', data)



def calendariomedico(request):
    agendas = agenda.objects.all()
    data = {
        'agenda':agendas
    }
    return render(request, 'core/calendariomedico.html',data)



def listado_pago(request):
    pago = Pago.objects.all()

    data = {
        'pago': pago
    }
    return render(request, 'core/listado_pago.html', data)


def eliminar_reserva(request, id):
    reserva = get_object_or_404(Reserva, id =id)
    reserva.delete()
    return redirect(to="listado_reserva")

def fullcalendar(request):

    data = {
        'form': fullcalendarForm(),
    }

    if request.method == 'POST':
        formuluario = fullcalendarForm(data=request.POST)
        if formuluario.is_valid():
            formuluario.save()
            data["mensaje"] = "Reserva guardada correctamente"
        else:
            data["form"] = formuluario
    return render(request, 'core/fullcalendar.html', data)

